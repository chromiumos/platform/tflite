# ChromeOS Sample Delegate

This is a sample stable delegate tailored for ChromeOS. Some implementation are
adapted from the sample in upstream TensorFlow
https://github.com/tensorflow/tensorflow/tree/HEAD/tensorflow/lite/delegates/utils/experimental/sample_stable_delegate.
